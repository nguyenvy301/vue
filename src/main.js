import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

Vue.config.productionTip = false

// use global filter
Vue.filter('lowerCase', function(value) {
    return value.toLowerCase();
});

new Vue({
    router,
    store,
    // Cho phép ứng dụng sử dụng store
    render: h => h(App)
}).$mount('#app')