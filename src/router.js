import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import DataMethod from './views/DataAndMethod.vue'
import Directive from './views/VueDirective.vue'
import Computed from './views/VueComputed.vue'
import ClassStyle from './views/VueClass.vue';
import FormInput from './views/VueForm.vue'
import ComponentVue from './views/VueComponents.vue'
import FilterMixins from './views/VueFilterMixins.vue'
import UseVuex from './views/Vuex.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [{
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () =>
                import ( /* webpackChunkName: "about" */ './views/About.vue')
        },
        {
            path: '/datamethod',
            name: 'datamethod',
            component: DataMethod
        },
        {
            path: '/directive',
            name: 'directive',
            component: Directive
        },
        {
            path: '/computed',
            name: 'computed',
            component: Computed
        },
        {
            path: '/formInput',
            name: 'formInput',
            component: FormInput
        },
        {
            path: '/component',
            name: 'component',
            component: ComponentVue
        },
        {
            path: '/filterMixins',
            name: 'filterMixins',
            component: FilterMixins
        },
        {
            path: '/classStyle',
            name: 'classStyle',
            component: ClassStyle
        },
        {
            path: '/useVuex',
            name: 'useVuex',
            component: UseVuex
        }
    ]
})