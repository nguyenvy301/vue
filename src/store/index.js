import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        user: {},
        count: 0
    },
    getters: {
        isLoggedin: state => {
            return !!state.user.username
        },
        doubleCount: state => {
            return state.count * 2;
        }
    },
    mutations: {
        login(state, data) {
            state.user = data
        },
        logout(state) {
            state.user = {}
        },
        increRandom(state, val) {
            state.count += val
        },
        decreTen(state) {
            state.count -= 10
        },
        increment(state) {
            state.count++
        }
    },
    actions: {
        login({ commit }, data) {
            commit('login', data)
        },
        incrementAc: ({ commit }) => {
            commit('increment')
        },
        decrementAc: ({ commit }) => {
            setTimeout(() => {
                commit('decreTen')
            }, 3000)
        }
    }
})